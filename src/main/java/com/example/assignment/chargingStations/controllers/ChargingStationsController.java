package com.example.assignment.chargingStations.controllers;


import com.example.assignment.chargingStations.models.response.ChargingStation;
import com.example.assignment.chargingStations.services.OpenChargeMapService;
import com.example.assignment.chargingStations.services.SecurityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class ChargingStationsController {

    private final OpenChargeMapService openChargeMapService;

    private final SecurityService securityService;

    public ChargingStationsController(OpenChargeMapService openChargeMapService, SecurityService securityService) {
        this.openChargeMapService = openChargeMapService;
        this.securityService = securityService;
    }

    @RequestMapping(path = "/nearest-charging-stations", method = RequestMethod.GET)
    public ResponseEntity<List<ChargingStation>> getNearestChargingStations(@RequestHeader(value = "Authorization", required = false) String token,
                                                                            @RequestParam Double latitude,
                                                                            @RequestParam Double longitude) {
        checkToken(token);
        securityService.validateToken(token);
        List<ChargingStation> nearestChargingStationRequests = openChargeMapService.getNearestChargingStations(latitude, longitude);
        return new ResponseEntity<>(nearestChargingStationRequests, HttpStatus.OK);
    }

    private void checkToken(String token) {
        if (token == null || token.equals("")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Authorization header is missing.");
        }
    }
}
