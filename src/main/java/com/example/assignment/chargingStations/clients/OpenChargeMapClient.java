package com.example.assignment.chargingStations.clients;

import com.example.assignment.chargingStations.models.openChargeMap.OpenChargeMapStations;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class OpenChargeMapClient {

    private final String openChargeMapApiKey = "d67d9772-1d6e-478a-ad5b-f5b1e720953f";
    private final String openChargeMapKeyHeader = "X-API-Key";
    private final String openChargeMapUrl = "https://api.openchargemap.io/v3/poi/";
    private final int chargingStationsResults = 10;
    private final RestTemplate restTemplate;

    @Autowired
    public OpenChargeMapClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Cacheable("openChargeMap")
    public List<OpenChargeMapStations> getNearestChargingStations(String latitude, String longitude) {
        HttpEntity<OpenChargeMapStations[]> entity = new HttpEntity<>(getHeaders());
        String openChargeMapUrl = getOpenChargeMapUrl(latitude, longitude);
        ResponseEntity<OpenChargeMapStations[]> chargingStations;
        try {
            chargingStations = restTemplate.exchange(openChargeMapUrl, HttpMethod.GET, entity,
                    OpenChargeMapStations[].class);
        } catch (Exception ex) {
            log.error("Error fetching data from the OpenChargeMap");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    String.format("Error obtaining data from OpenChargeMap: %s", ex.getMessage()));
        }
        return new ArrayList<>(Arrays.asList(chargingStations.getBody()));
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set(openChargeMapKeyHeader, openChargeMapApiKey);
        return headers;
    }

    private String getOpenChargeMapUrl(String latitude, String longitude) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(openChargeMapUrl)
                .queryParam("output", "json")
                .queryParam("maxresults", chargingStationsResults)
                .queryParam("latitude", latitude)
                .queryParam("longitude", longitude);
        return uriBuilder.toUriString();
    }
}
