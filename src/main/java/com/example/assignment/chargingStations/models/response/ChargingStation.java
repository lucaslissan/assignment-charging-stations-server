package com.example.assignment.chargingStations.models.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChargingStation {
    private String title;
    private Double latitude;
    private Double longitude;
}
