package com.example.assignment.chargingStations.models.openChargeMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OpenChargeMapStations {
    public AddressInfo AddressInfo;

}
