package com.example.assignment.chargingStations.models.openChargeMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressInfo {
    public String Title;
    public String Town;
    public String PostCode;
    public Double Latitude;
    public Double Longitude;
}
