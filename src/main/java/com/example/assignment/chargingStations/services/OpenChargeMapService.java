package com.example.assignment.chargingStations.services;

import com.example.assignment.chargingStations.clients.OpenChargeMapClient;
import com.example.assignment.chargingStations.models.openChargeMap.OpenChargeMapStations;
import com.example.assignment.chargingStations.models.response.ChargingStation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class OpenChargeMapService {

    private static final Double MAX_LATITUDE = 90.0000000;
    private static final Double MIN_LATITUDE = -90.0000000;
    private static final Double MAX_LONGITUDE = 180.0000000;
    private static final Double MIN_LONGITUDE = -180.0000000;
    private final OpenChargeMapClient openChargeMapClient;

    public OpenChargeMapService(OpenChargeMapClient openChargeMapClient) {
        this.openChargeMapClient = openChargeMapClient;
    }

    public List<ChargingStation> getNearestChargingStations(Double latitude, Double longitude) {
        validateLatitudeLongitude(latitude, longitude);
        List<ChargingStation> chargingStationList = new ArrayList<>();

        List<OpenChargeMapStations> openChargeMapStations = openChargeMapClient.getNearestChargingStations(formatLatitude(latitude),
                formatLongitude(longitude));
        if(openChargeMapStations.size() == 0){
            log.info(String.format("Charging stations not found for latitude %s and longitude %s", latitude, longitude));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Charging stations not found for latitude %s and longitude %s", latitude, longitude));
        }
        openChargeMapStations.forEach(station -> {
            chargingStationList.add(ChargingStation.builder()
                    .latitude(station.AddressInfo.Latitude)
                    .longitude(station.AddressInfo.Longitude)
                    .title(station.AddressInfo.Title)
                    .build());
        });
        return chargingStationList;
    }

    private String formatLatitude(Double latitude) {
        return String.format("%.2f", latitude);
    }

    private String formatLongitude(Double longitude) {
        return String.format("%.2f", longitude);
    }

    private void validateLatitudeLongitude(Double latitude, Double longitude) {
        if (isLatitudeOutOfRange(latitude) || isLongitudeOutOfRange(longitude)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Values for latitude or longitude are out of range.");
        }
    }

    private boolean isLatitudeOutOfRange(Double latitude) {
        return (latitude.compareTo(MIN_LATITUDE) < 0) || (latitude.compareTo(MAX_LATITUDE) > 0);
    }

    private boolean isLongitudeOutOfRange(Double longitude) {
        return (longitude.compareTo(MIN_LONGITUDE) < 0) || (longitude.compareTo(MAX_LONGITUDE) > 0);
    }
}
