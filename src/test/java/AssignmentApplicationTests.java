import com.example.assignment.AssignmentApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AssignmentApplication.class)
class AssignmentApplicationTests {

    @Test
    void contextLoads() {
    }

}
