package com.example.assignmentchargingstations.clients;


import com.example.assignment.chargingStations.clients.OpenChargeMapClient;
import com.example.assignment.chargingStations.models.openChargeMap.AddressInfo;
import com.example.assignment.chargingStations.models.openChargeMap.OpenChargeMapStations;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class OpenChargeMapClientTest {

    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private OpenChargeMapClient openChargeMapClient;

    @Test
    public void testGetNearestChargingStations() {
        Double latitude = 90.0;
        Double longitude = 90.0;
        String urlExpected = getUrlExpected(latitude, longitude);
        HttpEntity<OpenChargeMapStations[]> entityExpected = new HttpEntity<OpenChargeMapStations[]>(getHeadersExpected());
        OpenChargeMapStations[] chargingStationsListRequest = getChargingStationsListMock(latitude, longitude);

        when(restTemplate.exchange(anyString(), any(), any(), (Class<Object>) any()))
                .thenReturn(new ResponseEntity(chargingStationsListRequest, HttpStatus.OK));

        openChargeMapClient.getNearestChargingStations(latitude.toString(), longitude.toString());

        verify(restTemplate, times(1)).exchange(urlExpected, HttpMethod.GET,
                entityExpected, OpenChargeMapStations[].class);
    }

    @Test
    public void testGetNearestChargingStationsThrowsInternalServerErrorException() {
        Double latitude = 90.0;
        Double longitude = 90.0;

        when(restTemplate.exchange(anyString(), any(), any(), (Class<Object>) any()))
                .thenThrow(new RuntimeException());
        try {
            openChargeMapClient.getNearestChargingStations(latitude.toString(), longitude.toString());
        } catch (ResponseStatusException ex) {
            assert (ex.getMessage().contains("Error obtaining data from OpenChargeMap:"));
            Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getStatus().value());
        }
    }


    private OpenChargeMapStations[] getChargingStationsListMock(Double latitude, Double longitude) {
        OpenChargeMapStations[] chargingStationsArrayRequest = new OpenChargeMapStations[1];
        AddressInfo addressInfoExpected = AddressInfo.builder().Latitude(latitude).Longitude(longitude).Title("Test Charging Station").build();
        chargingStationsArrayRequest[0] = OpenChargeMapStations.builder().AddressInfo(addressInfoExpected).build();
        return chargingStationsArrayRequest;
    }

    private String getUrlExpected(Double latitude, Double longitude) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl("https://api.openchargemap.io/v3/poi/")
                .queryParam("output", "json")
                .queryParam("maxresults", 10)
                .queryParam("latitude", formatLatitude(latitude))
                .queryParam("longitude", formatLongitude(longitude));
        return uriBuilder.toUriString();
    }

    private Double formatLatitude(Double latitude) {
        return Math.floor(latitude * 1e2) / 1e2;
    }

    private Double formatLongitude(Double longitude) {
        return Math.floor(longitude * 1e2) / 1e2;
    }

    private HttpHeaders getHeadersExpected() {
        HttpHeaders headersExpected = new HttpHeaders();
        headersExpected.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headersExpected.set("X-API-Key", "d67d9772-1d6e-478a-ad5b-f5b1e720953f");
        return headersExpected;
    }

}
