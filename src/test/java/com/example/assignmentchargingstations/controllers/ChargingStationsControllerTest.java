package com.example.assignmentchargingstations.controllers;


import com.example.assignment.chargingStations.controllers.ChargingStationsController;
import com.example.assignment.chargingStations.models.response.ChargingStation;
import com.example.assignment.chargingStations.services.OpenChargeMapService;
import com.example.assignment.chargingStations.services.SecurityService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChargingStationsControllerTest {

    @Mock
    private OpenChargeMapService openChargeMapService;
    @Mock
    private SecurityService securityService;
    @InjectMocks
    private ChargingStationsController chargingStationsController;

    private String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXRoMCIsImlhdCI6MTYyNDE1ODU5NSwiZXhwIjo0MDg1Njk0NTk1LCJhdWQiOiJ3d3cuZXhhbXBsZS5jb20iLCJzdWIiOiJqcm9ja2V0QGV4YW1wbGUuY29tIiwiR2l2ZW5OYW1lIjoiSm9obm55IiwiU3VybmFtZSI6IlJvY2tldCIsIkVtYWlsIjoianJvY2tldEBleGFtcGxlLmNvbSIsIlJvbGUiOlsiTWFuYWdlciIsIlByb2plY3QgQWRtaW5pc3RyYXRvciJdfQ.7oAf4OKPmE4rXJdEfjhMBlei9O9OS_6qykbj4_thxRw";

    @Test
    public void testGetNearestChargingStations() {
        Double latitude = 90.0;
        Double longitude = 90.0;
        List<ChargingStation> chargingStationListExpected = new ArrayList<>();
        chargingStationListExpected.add(ChargingStation.builder().title("Test Charging Station")
                .longitude(longitude)
                .latitude(latitude)
                .build());

        doNothing().when(securityService).validateToken(token);
        when(openChargeMapService.getNearestChargingStations(anyDouble(), anyDouble())).thenReturn(chargingStationListExpected);

        ResponseEntity<List<ChargingStation>> responseExpected = chargingStationsController.getNearestChargingStations(token, latitude, longitude);

        Assertions.assertEquals(responseExpected.getStatusCode(), HttpStatus.OK);
        Assertions.assertEquals(responseExpected.getBody(), chargingStationListExpected);
    }

    @Test(expected = HttpClientErrorException.NotFound.class)
    public void testGetNearestChargingStationsReturnsNoOkStatus() {
        Double latitude = 90.0;
        Double longitude = 90.0;

        doNothing().when(securityService).validateToken(token);
        when(openChargeMapService.getNearestChargingStations(anyDouble(), anyDouble())).thenThrow(HttpClientErrorException.NotFound.class);

        ResponseEntity<List<ChargingStation>> responseExpected = chargingStationsController.getNearestChargingStations(token, latitude, longitude);
        Assertions.assertEquals(responseExpected.getStatusCode(), HttpStatus.NOT_FOUND);
    }
}
