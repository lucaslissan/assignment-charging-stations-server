package com.example.assignmentchargingstations.services;


import com.example.assignment.chargingStations.clients.OpenChargeMapClient;
import com.example.assignment.chargingStations.models.openChargeMap.AddressInfo;
import com.example.assignment.chargingStations.models.openChargeMap.OpenChargeMapStations;
import com.example.assignment.chargingStations.models.response.ChargingStation;
import com.example.assignment.chargingStations.services.OpenChargeMapService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChargingStationsServiceTest {

    @Mock
    private OpenChargeMapClient openChargeMapClient;
    @InjectMocks
    private OpenChargeMapService openChargeMapService;

    @Test
    public void testGetNearestChargingStations() {
        Double latitude = 90.0;
        Double longitude = 90.0;
        List<OpenChargeMapStations> openChargeMapStationsList = new ArrayList<>();
        AddressInfo addressInfoExpected = AddressInfo.builder().Latitude(latitude).Longitude(longitude).Title("Test Charging Station").build();
        openChargeMapStationsList.add(OpenChargeMapStations.builder().AddressInfo(addressInfoExpected).build());
        List<ChargingStation> chargingStationListExpected = new ArrayList<>();
        chargingStationListExpected.add(ChargingStation.builder().title("Test Charging Station")
                .longitude(longitude)
                .latitude(latitude)
                .build());
        when(openChargeMapClient.getNearestChargingStations(anyString(), anyString())).thenReturn(openChargeMapStationsList);

        List<ChargingStation> chargingStationList = openChargeMapService.getNearestChargingStations(latitude, longitude);
        Assertions.assertEquals(chargingStationList, chargingStationListExpected);
    }

    @Test
    public void testGetNearestChargingStationsForEmptyArrayThrowsNotFoundException() {
        Double latitude = 90.0;
        Double longitude = 90.0;
        List<OpenChargeMapStations> openChargeMapStationsList = new ArrayList<>();
        when(openChargeMapClient.getNearestChargingStations(anyString(), anyString())).thenReturn(openChargeMapStationsList);

        try {
            openChargeMapService.getNearestChargingStations(latitude, longitude);
        } catch (ResponseStatusException ex) {
            assert (ex.getMessage().contains(String.format("Charging stations not found for latitude %s and longitude %s", latitude, longitude)));
            Assert.assertEquals(HttpStatus.NOT_FOUND.value(), ex.getStatus().value());
        }
    }

    @Test
    public void testGetNearestChargingStationsForInvalidValuesThrowsBadRequestException() {
        Double latitude = 100.0;
        Double longitude = 185.0;
        try {
            openChargeMapService.getNearestChargingStations(latitude, longitude);
        } catch (ResponseStatusException ex) {
            assert (ex.getMessage().contains("Values for latitude or longitude are out of range"));
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), ex.getStatus().value());
        }
    }

    @Test
    public void testGetNearestChargingStationsForInvalidLatitudeThrowsBadRequestException() {
        Double latitude = 100.0;
        Double longitude = 70.0;
        try {
            openChargeMapService.getNearestChargingStations(latitude, longitude);
        } catch (ResponseStatusException ex) {
            assert (ex.getMessage().contains("Values for latitude or longitude are out of range"));
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), ex.getStatus().value());
        }
    }

    @Test
    public void testGetNearestChargingStationsForInvalidLongitudeThrowsBadRequestException() {
        Double latitude = 80.0;
        Double longitude = 190.0;
        try {
            openChargeMapService.getNearestChargingStations(latitude, longitude);
        } catch (ResponseStatusException ex) {
            assert (ex.getMessage().contains("Values for latitude or longitude are out of range"));
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), ex.getStatus().value());
        }
    }
}
